﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotater : MonoBehaviour
{
    [SerializeField] private Vector3 m_Speeds;
    [SerializeField] private Vector3 m_Magnitudes;
    [SerializeField] private Transform m_Movee;

    [SerializeField] private bool m_SinX = true;
    [SerializeField] private bool m_SinY = true;
    [SerializeField] private bool m_SinZ = true;

    private Vector3 m_thetas;

    private void Update()
    {
        m_thetas.x = (m_thetas.x + Time.deltaTime * m_Speeds.x) % 1f;
        m_thetas.y = (m_thetas.y + Time.deltaTime * m_Speeds.y) % 1f;
        m_thetas.z = (m_thetas.z + Time.deltaTime * m_Speeds.z) % 1f;

        /*
        m_Movee.localEulerAngles = transform.localEulerAngles + new Vector3(
            m_SinX ? Mathf.Sin(m_thetas.x * Mathf.PI * 2f) * m_Magnitudes.x : Mathf.Cos(m_thetas.x * Mathf.PI * 2f) * m_Magnitudes.x,
            m_SinY ? Mathf.Sin(m_thetas.y * Mathf.PI * 2f) * m_Magnitudes.y : Mathf.Cos(m_thetas.y * Mathf.PI * 2f) * m_Magnitudes.y,
            m_SinZ ? Mathf.Sin(m_thetas.z * Mathf.PI * 2f) * m_Magnitudes.z : Mathf.Cos(m_thetas.z * Mathf.PI * 2f) * m_Magnitudes.z);
            */

        m_Movee.localEulerAngles = transform.localEulerAngles + m_thetas * 360f;
    }
}
