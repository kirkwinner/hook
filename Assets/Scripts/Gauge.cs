﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gauge : MonoBehaviour
{
    public RectTransform m_Max;
    public RectTransform m_Min;
    public RectTransform m_Bar;

    public void SetVal(float val)
    {
        m_Bar.anchoredPosition = Vector2.Lerp(m_Min.anchoredPosition, m_Max.anchoredPosition, val);
    }
}
