﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Motor : IMotor
{
    public IDriveable Vehicle { get; set; }

    public void DoDrive(Vector3 velocity)
    {
        if (Vehicle != null)
            Vehicle.Drive(velocity);
    }
}

public interface IMotor
{
    IDriveable Vehicle { get; set; }

    void DoDrive(Vector3 velocity);
}