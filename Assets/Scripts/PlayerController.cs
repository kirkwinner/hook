﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerController : MonoBehaviour
{
    private enum PlayerState
    {
        Idle = 0,
        Running = 1,
        Charging = 2,
        Launching = 3,
        Posing = 4,
    }

    private PlayerState m_State;
    private NavMeshAgent m_Agent;
    private Animator m_Animator;

    private Vector3 m_CursorPos;
    private Vector3 m_PrevCursorPos;

    private float m_Power;
    private float m_Height;
    private float m_Angle;

    [SerializeField] private LayerMask m_LayerMask;

    [SerializeField] private Vector2 m_PowerMinMax = new Vector2(1f, 20f);
    [SerializeField] private float m_PowerChange = 2f;
    [SerializeField] private Vector2 m_HeightMinMax = new Vector2(0f, 0.24f);
    [SerializeField] private float m_HeightChange = 0.1f;
    [SerializeField] private float m_AngleChange = 0.2f;
    [SerializeField] private Bobber m_Bobber;
    [SerializeField] private Transform m_LaunchPoint;
    [SerializeField] private float m_speed = 10f;

    [SerializeField] private LineRenderer m_Line;
    [SerializeField] private Transform m_Target;
    [SerializeField] private Transform m_TargetRoot;
    [SerializeField] private Transform m_PosePos;

    [SerializeField] private RandomAudioPlayer m_FootstepAudio;

    [SerializeField] private GameManager m_Manager;

    private Vector3 m_Velocity;
    private bool m_CanControl = true;

    private Fish m_Fish;

    public void PlayFootstep()
    {
        m_FootstepAudio.PlayRandom();
    }

    void Start()
    {
        m_Agent = GetComponent<NavMeshAgent>();
        m_Animator = GetComponent<Animator>();
        SetState(PlayerState.Idle);

        m_Angle = 0.25f;
        m_Height = 0.125f;
        m_Power = 3f;
    }


    float mod(float x, float m)
    {
        float r = x % m;
        return r < 0 ? r + m : r;
    }
    // Update is called once per frame

    void SetState(PlayerState newState)
    {
        m_State = newState;

        m_Animator.SetInteger("playerState", (int)m_State);
    }

    float m_velocityLastUpdated = 0f;

    void Update()
    {
        if (!m_CanControl)
            return;

        if(m_State == PlayerState.Launching)
        {
            if(!m_Bobber.m_IsRetracting && Input.GetKey(KeyCode.Space))
            {
                m_Bobber.Retract(null);
            }

            return;
        }

        if(m_State == PlayerState.Posing)
        {
            if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.RightArrow))
            {
                SetState(PlayerState.Idle);

                if(m_Fish)
                {
                    m_Fish.gameObject.SetActive(false);
                    m_Fish = null;
                }
            }

            return;
        }

        var movement = Vector3.zero;

        if (Input.GetKey(KeyCode.Space))
        {
            var flattenedPlayer = new Vector3(transform.position.x, 0f, transform.position.z);

            bool m_ValuesChanged = false;

            if(m_State != PlayerState.Charging)
            {
                //m_Angle = 0.25f;
                //m_Height = 0.125f;
                //m_Power = 3f;
                m_Velocity = Vector3.zero;
                m_ValuesChanged = true;
                SetState(PlayerState.Charging);

                m_Bobber.gameObject.SetActive(false);
                m_Target.gameObject.SetActive(true);

                m_Manager.m_IsUsingControls = true;
            }

            //Power
            if (Input.GetKey(KeyCode.Q)) { m_Power += m_PowerChange * Time.deltaTime; m_ValuesChanged = true; }
            if (Input.GetKey(KeyCode.A)) { m_Power -= m_PowerChange * Time.deltaTime; m_ValuesChanged = true; }

            m_Power = Mathf.Clamp(m_Power, m_PowerMinMax.x, m_PowerMinMax.y);

            //Height
            if (Input.GetKey(KeyCode.W)) { m_Height += m_HeightChange * Time.deltaTime; m_ValuesChanged = true; }
            if (Input.GetKey(KeyCode.S)) { m_Height -= m_HeightChange * Time.deltaTime; m_ValuesChanged = true; }

            m_Height = Mathf.Clamp(m_Height, m_HeightMinMax.x, m_HeightMinMax.y);

            //Angle
            if (Input.GetKey(KeyCode.E)) { m_Angle += m_AngleChange * Time.deltaTime; m_ValuesChanged = true; }
            if (Input.GetKey(KeyCode.D)) { m_Angle -= m_AngleChange * Time.deltaTime; m_ValuesChanged = true; }

            m_Angle = mod(m_Angle, 1f);


            m_Manager.SetValues(Mathf.InverseLerp(m_PowerMinMax.x, m_PowerMinMax.y, m_Power),
                Mathf.InverseLerp(m_HeightMinMax.x, m_HeightMinMax.y, m_Height),
                m_Angle);

            var flattenedCursor = new Vector3(m_CursorPos.x, 0f, m_CursorPos.z);

            if(m_ValuesChanged || m_velocityLastUpdated < Time.time)
            {
                //Update target

                m_Velocity = SetTarget();

                m_velocityLastUpdated = Time.time + 0.2f;
            }
        }
        else
        {
            m_Manager.m_IsUsingControls = false;

            if (m_State == PlayerState.Charging)
            {
                m_Target.gameObject.SetActive(false);

                StartCoroutine(WaitToLaunch());

                return;
            }

            if (Input.GetKey(KeyCode.LeftArrow)) { movement += Vector3.left; }
            if (Input.GetKey(KeyCode.RightArrow)) { movement += Vector3.right; }
            if (Input.GetKey(KeyCode.UpArrow)) { movement += Vector3.forward; }
            if (Input.GetKey(KeyCode.DownArrow)) { movement += Vector3.back; }

            m_Agent.Move(m_speed * movement.normalized * Time.deltaTime);

            if (movement.magnitude > 0.1f)
            {
                if(m_Bobber.gameObject.active)
                {
                    m_Bobber.gameObject.SetActive(false);
                }

                SetState( PlayerState.Running);
            }
            else if(m_State == PlayerState.Running)
            {
                SetState(PlayerState.Idle);
            }
        }
    }

    private IEnumerator WaitToLaunch()
    {
        m_CanControl = false;

        yield return new WaitForSeconds(1f);

        SetState(PlayerState.Launching);


        m_Bobber.Setup(m_Velocity, m_LaunchPoint.position, OnRetracted);

        m_CanControl = true;
    }

    private void OnRetracted(Fish fish)
    {
        if(fish != null)
        {
            m_Fish = fish;
            m_Fish.m_IsCatching = false;
            m_Fish.m_IsCaught = true;
            m_Fish.transform.position = m_PosePos.position;
            m_Fish.m_TargetRotation = m_PosePos.rotation;
            SetState(PlayerState.Posing);

            m_Manager.PlayTada();
        }
        else
        {
            SetState(PlayerState.Idle);
        }

        m_Bobber.gameObject.SetActive(false);
    }

    private Vector3 SetTarget()
    {
        //s = ut + 1/2 at

        //position = velocity * time + 0.5f * gravity * time * time

        var velocity = Vector3.right;

        velocity = Quaternion.Euler(-m_Height * 360f, m_Angle * 360f, 0f) * Vector3.forward * m_Power;

        //Quaternion.AngleAxis(m_Angle * 360f, Vector3.up) * Quaternion.AngleAxis(-m_Height * 360f, Vector3.forward)

        List<Vector3> points = new List<Vector3>();

        var initialPos = m_LaunchPoint.position;

        RaycastHit hit;

        var prevPos = initialPos;

        var finalPos = initialPos;
        var finalUp = Vector3.up;

        for (float tick = 0f; tick < 5f; tick += 1f/30f)
        {
            var positionAtTime = initialPos + velocity * tick + 0.5f * Physics.gravity * tick * tick;

            var dif = (positionAtTime - prevPos);
            var normal = dif.normalized;
            var mag = dif.magnitude;

            if (Physics.SphereCast(prevPos, 0.05f, normal, out hit, mag, m_LayerMask))
            {
                finalUp = hit.normal;
                finalPos = hit.point;

                points.Add(finalPos);
                break;
            }

            prevPos = positionAtTime;

            finalPos = positionAtTime;
            points.Add(finalPos);
        }

        m_TargetRoot.position = finalPos;
        m_TargetRoot.up = finalUp;

        m_Line.positionCount = points.Count;
        m_Line.SetPositions(points.ToArray());

        return velocity;
    }
}
