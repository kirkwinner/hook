﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class UnityEventOnScoreChange : MonoBehaviour
{
    public UnityEvent m_DoOnScoreChange;

    public int m_Score;

    public ScoreType m_Type;

    public enum ScoreType
    {
        GreaterThan,
        GreaterThanOrEqualTo,
        EqualTo,
        LessThanOrEqualTo,
        LessThan
    }

    private void OnEnable()
    {
        //GameManager.s_DoOnScoreChange -= OnScoreChange;
        GameManager.s_DoOnScoreChange += OnScoreChange;
    }

    private void OnScoreChange(int score)
    {
        /*
        Mesh mesh = new Mesh();

        var verts = mesh.vertices;
        var uvs = mesh.uv;

        var newUvs = new Vector2[uvs.Length];

        var worldSpaceDist = Vector3.Distance(verts[0], verts[1]);
        var uvDist = Vector2.Distance(uvs[0], uvs[1]);

        var ratio = worldSpaceDist / uvDist;

        for(int i = 0; i < uvs.Length; i++)
        {
            newUvs[i] = uvs[i] * ratio;
        }

        mesh.uv = newUvs;

    */





        switch (m_Type)
        {
            case ScoreType.GreaterThan: if (score > m_Score) { m_DoOnScoreChange?.Invoke(); } break;
            case ScoreType.GreaterThanOrEqualTo: if (score >= m_Score) {m_DoOnScoreChange?.Invoke();} break;
            case ScoreType.EqualTo: if (score == m_Score) {m_DoOnScoreChange?.Invoke(); } break;
            case ScoreType.LessThanOrEqualTo: if (score <= m_Score){ m_DoOnScoreChange?.Invoke(); }break;
            case ScoreType.LessThan: if (score < m_Score) {m_DoOnScoreChange?.Invoke();} break;
        }
    }

    private void OnDisable()
    {
        GameManager.s_DoOnScoreChange -= OnScoreChange;
    }
}
