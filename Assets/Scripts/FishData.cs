﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewFish", menuName = "Data/Create Fish", order = 1)]
public class FishData : ScriptableObject
{
    public Texture2D m_Image;
    public string m_Creator;
    public string m_FishName;
}
