﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomAudioPlayer : MonoBehaviour
{
    [SerializeField] private AudioClip[] m_Clips;
    [SerializeField] private AudioSource m_Source;


    public void PlayRandom()
    {
        if (m_Clips.Length == 0)
            return;

        var clip = m_Clips[Random.Range(0, m_Clips.Length - 1)];

        if (clip == null)
            return;

        m_Source.PlayOneShot(clip);
    }
}
