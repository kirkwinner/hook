﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    [SerializeField] private Sprite[] m_Values;

    [SerializeField] private Image[] m_Digits;

    private void Start()
    {
        SetScore(0);
    }

    public int[] NumbersIn(int value)
    {
        var numbers = new List<int>();

        for (; value > 0; value /= 10)
            numbers.Add(value % 10);

        return numbers.ToArray();
    }

    public void SetScore(int value)
    {
        var digits = NumbersIn(Mathf.Clamp(value, 0, 999));



        for(int i = 0; i < m_Digits.Length; i ++)
        {
            m_Digits[i].gameObject.SetActive(digits.Length >= i + 1);

            if(digits.Length >= i + 1)
            {
                m_Digits[i].sprite = m_Values[digits[i]];
            }
        }

        if (digits.Length == 0)
        {
            m_Digits[0].gameObject.SetActive(true);

            m_Digits[0].sprite = m_Values[0];
        }
    }
}
