﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldUVs : MonoBehaviour
{
    MeshFilter m_MeshFilter;

    Mesh m_Mesh;
    Vector2[] m_UVs;
    Vector2[] m_NewUvs;

    void Start()
    {
        m_MeshFilter = GetComponent<MeshFilter>();
        m_Mesh = m_MeshFilter.mesh;
        m_UVs = m_Mesh.uv;
        m_NewUvs = new Vector2[m_UVs.Length];
    }

    // Update is called once per frame
    void Update()
    {
        var offset = new Vector2(transform.position.x, transform.position.z);

        for (int i = 0; i < m_UVs.Length; i++)
        {
            m_NewUvs[i] = m_UVs[i] + offset;
        }

        m_Mesh.uv = m_NewUvs;
    }
}
