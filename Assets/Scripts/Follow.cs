﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follow : MonoBehaviour
{
    public Transform m_Target;

    public bool m_Lerp = false;
    public float m_LerpSpeed = 5f;

    public float m_HeightCap = -100f;

    private void OnEnable()
    {
        if (m_Target)
            transform.position = m_Target.position;
    }

    void Update()
    {
        var newPos = Vector3.zero;

        if(m_Lerp)
        {
            newPos = Vector3.Lerp(transform.position, m_Target.position, Time.deltaTime * m_LerpSpeed);

            if(newPos.y < m_HeightCap)
            {
                newPos.y = m_HeightCap;
            }

            transform.position = newPos;

            return;
        }

        if (m_Target)
        {
            newPos = m_Target.position;

            if (newPos.y < m_HeightCap)
            {
                newPos.y = m_HeightCap;
            }

            transform.position = newPos;

        }

        transform.position = newPos;
    }
}
