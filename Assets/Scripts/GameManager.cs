﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    bool m_HasBegun = false;

    public float m_FadeTime = 3f;
    public GameObject m_StartCam;
    public CanvasGroup m_Fade;
    public AudioSource m_Music;
    public AudioClip m_Tada;

    public Gauge m_Power;
    public Gauge m_Height;
    public RectTransform m_Angle;

    public CanvasGroup m_ControlFade;
    public CanvasGroup m_ShootFade;

    public bool m_IsUsingControls = false;

    public Score m_Score;

    private int m_ScoreValue;

    public static System.Action<int> s_DoOnScoreChange;

    void Update()
    {
        if(m_IsUsingControls && m_ControlFade.alpha < 1f)
        {
            m_ControlFade.alpha += Time.deltaTime * 10f;
        }

        if (!m_IsUsingControls && m_ControlFade.alpha > 0f)
        {
            m_ControlFade.alpha -= Time.deltaTime * 10f;
        }



        if (m_HasBegun)
        {
            if (m_IsUsingControls && m_ShootFade.alpha > 0f)
            {
                m_ShootFade.alpha -= Time.deltaTime * 10f;
            }

            if (!m_IsUsingControls && m_ShootFade.alpha < 1f)
            {
                m_ShootFade.alpha += Time.deltaTime * 10f;
            }

            return;
        }

        if (Input.anyKeyDown)
        {
            m_HasBegun = true;

            m_StartCam.SetActive(false);

            StartCoroutine(TitleFade());

            m_Music.Play();
        }
    }

    public void SetValues(float power, float height, float angle)
    {
        m_Power.SetVal(power);
        m_Height.SetVal(height);
        m_Angle.localEulerAngles = new Vector3(0f, 0f, -angle * 360f);
    }

    public void PlayTada()
    {
        m_Music.PlayOneShot(m_Tada);

        IncreaseScore();
    }

    private void IncreaseScore()
    {
        m_ScoreValue += 1;
        m_Score.SetScore(m_ScoreValue);

        s_DoOnScoreChange?.Invoke(m_ScoreValue);
    }

    private IEnumerator TitleFade()
    {

        var time = Time.time + m_FadeTime;

        while(Time.time < m_FadeTime)
        {
            m_Fade.alpha = ((time - Time.time) / m_FadeTime);

            yield return null;
        }

        m_Fade.alpha = 0f;

        m_Fade.gameObject.SetActive(false);
    }
}
