﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Counter : MonoBehaviour
{
    [SerializeField] private int m_GridSize;

    [SerializeField] private Mesh m_RootMesh;

    private Mesh m_OnesMesh;
    private Mesh m_TensMesh;
    private Mesh m_HundredsMesh;

    [SerializeField] private MeshFilter m_Ones;
    [SerializeField] private MeshFilter m_Tens;
    [SerializeField] private MeshFilter m_Hundreds;

    public int CounterValue = 0;

    private void Start()
    {
        m_OnesMesh = Instantiate(m_RootMesh);
        m_TensMesh = Instantiate(m_RootMesh);
        m_HundredsMesh = Instantiate(m_RootMesh);

        SetCounter(CounterValue);
    }

    private Vector2[] m_Blanks = new Vector2[]
    {
        Vector2.zero,Vector2.zero,Vector2.zero,Vector2.zero
    };

    public int[] NumbersIn(int value)
    {
        var numbers = new List<int>();

        for (; value > 0; value /= 10)
            numbers.Add(value % 10);

        return numbers.ToArray();
    }

    private Vector2[] GetUVs(int position)
    {
        if(position >= 0 && position < m_GridSize * m_GridSize)
        {
            var gridWidth = 1f / (float)m_GridSize;
            var x = ((position % m_GridSize) / (float)m_GridSize);
            var y = ((position / m_GridSize) / (float)m_GridSize);

            return new Vector2[]
            {
                new Vector2(x, y), //Bottom left
                new Vector2(x + gridWidth, y), //Bottom right
                new Vector2(x, y + gridWidth), //Top left
                new Vector2(x + gridWidth, y + gridWidth), //Top right
            };
        }

        return m_Blanks;
    }

    public void SetCounter(int value)
    {
        var digits = NumbersIn(Mathf.Clamp(value, 0, 999));

        m_OnesMesh.uv = digits.Length > 0 ? GetUVs(digits[0]) : GetUVs(0);
        m_TensMesh.uv = digits.Length > 1 ? GetUVs(digits[1]) : m_Blanks;
        m_HundredsMesh.uv = digits.Length > 2 ? GetUVs(digits[2]) : m_Blanks;

        m_Ones.mesh = m_OnesMesh;
        m_Tens.mesh = m_TensMesh;
        m_Hundreds.mesh = m_HundredsMesh;
    }
}
