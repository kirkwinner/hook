﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class Actor : MonoBehaviour, IDriveable
{
    private NavMeshAgent m_Agent;

    public void Start()
    {
        m_Agent = GetComponent<NavMeshAgent>();
    }

    public void Drive(Vector3 velocity)
    {
        m_Agent.Move(velocity);
    }
}

public interface IDriveable
{
    void Drive(Vector3 velocity);
}