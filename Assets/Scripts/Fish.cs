﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fish : MonoBehaviour
{
    private Vector3 m_Direction;
    private float m_CurrentSpeed;
    private float m_DecreasePerSecond = 1f;
    private float m_Speed = 1f;
    private float m_TimeBetweenSpurts = 2f;

    private float m_TimeToStopWaiting;
    private float m_SniffDistance = 2f;

    private bool m_IsWaiting = false;
    public bool m_IsCatching = false;
    public bool m_IsCaught = false;

    [SerializeField] private RandomAudioPlayer m_Audio;
    [SerializeField] private LayerMask m_LayerMask;
    [SerializeField] private Transform m_Visuals;

    [SerializeField] private Bobber m_Bobber;

    public Quaternion m_TargetRotation = Quaternion.identity;
    public Transform m_Anchor;
    public FishData m_Data;

    public Material m_RootFrontMaterial;
    public Material m_RootBackMaterial;

    public MeshRenderer[] m_BackSprites;
    public MeshRenderer[] m_FrontSprites;

    // Start is called before the first frame update
    void Start()
    {


        if(m_Data != null)
        {
            SetupFishData();
        }

        RandomiseDirection();

        m_TimeToStopWaiting = Time.time + m_TimeBetweenSpurts + Random.Range(0f, m_TimeBetweenSpurts);
        m_IsCatching = false;
        m_IsCaught = false;
    }

    void SetupFishData()
    {
        var newFront = new Material(m_RootFrontMaterial);
        var newBack = new Material(m_RootBackMaterial);

        newFront.SetTexture("_MainTex", m_Data.m_Image);
        newBack.SetTexture("_MainTex", m_Data.m_Image);

        foreach (var renderer in m_FrontSprites)
        {
            renderer.material = newFront;
        }

        foreach(var renderer in m_BackSprites)
        {
            renderer.material = newBack;
        }
    }

    void RandomiseDirection()
    {
        var randomDir = Random.onUnitSphere;

        m_Direction = new Vector3(randomDir.x, 0f, randomDir.y).normalized;

        m_CurrentSpeed = m_Speed;

        m_Audio.PlayRandom();
    }

    private void LateUpdate()
    {
        if (m_IsCatching)
        {
            if ((m_Bobber.Position - transform.position).x > 0f)
            {
                m_TargetRotation = Quaternion.identity;
            }
            else
            {
                m_TargetRotation = Quaternion.AngleAxis(180f, Vector3.up);
            }

            transform.position = m_Bobber.Position;

            return;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(transform.rotation != m_TargetRotation)
            transform.rotation = Quaternion.Lerp(transform.rotation, m_TargetRotation, Time.deltaTime * 10f);

        if(m_IsCatching || m_IsCaught)
        {
            return;
        }

        if(m_Direction.x > 0f)
        {
            m_TargetRotation = Quaternion.identity;
        }
        else
        {
            m_TargetRotation = Quaternion.AngleAxis(180f, Vector3.up);
        }



        if (!m_IsWaiting)
        {

            if(Vector3.Distance(m_Bobber.Position, transform.position) < m_SniffDistance)
            {
                m_Direction = (m_Bobber.Position - transform.position);
                m_Direction.y = 0f;
                m_Direction = m_Direction.normalized;
            }

            if(Vector3.Distance(m_Anchor.position, transform.position) > m_Anchor.localScale.x * 0.5f)
            {
                m_Direction = (m_Anchor.position - transform.position);
                m_Direction.y = 0f;
                m_Direction = m_Direction.normalized;
            }

            transform.position += m_Direction * m_CurrentSpeed * Time.deltaTime;


            transform.position = new Vector3(transform.position.x, m_Anchor.position.y, transform.position.z);

            if (Vector3.Distance(m_Bobber.Position, transform.position) < 0.2f && !m_Bobber.m_IsRetracting)
            {
                m_IsCatching = true;

                m_Bobber.Retract(this);

                return;
            }


            m_CurrentSpeed -= m_DecreasePerSecond * Time.deltaTime;

            if (m_CurrentSpeed <= 0f)
            {
                m_IsWaiting = true;

                m_TimeToStopWaiting = Time.time + m_TimeBetweenSpurts + Random.Range(0f, m_TimeBetweenSpurts);
            }
        }
        else
        {
            if(Time.time >= m_TimeToStopWaiting)
            {
                m_IsWaiting = false;

                RandomiseDirection();
            }
        }
    }
}
