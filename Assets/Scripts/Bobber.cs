﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bobber : MonoBehaviour
{
    [SerializeField] private Rigidbody m_Rigidbody;
    [SerializeField] private LineRenderer m_LineRenderer;
    [SerializeField] private AudioSource m_Plunk;
    [SerializeField] private AudioSource m_Whirr;

    private Vector3 m_InitialPosition;

    private Vector3 m_PrevPos;

    private List<Vector3> m_Positions = new List<Vector3>();
    private float m_MaxDist = 0.05f;

    internal void PlayPlunk()
    {
        m_Plunk.Play();
    }

    public Vector3 Position;

    public Fish m_CaughtFish;

    public bool m_IsRetracting = false;

    private System.Action<Fish> m_OnRetract;

    private int m_prevLength = 0;

    public void Setup(Vector3 initialVelocity, Vector3 initialPosition, System.Action<Fish> onRetract)
    {
        m_Whirr.Stop();
        m_Rigidbody.isKinematic = false;
        m_IsRetracting = false;
        m_CaughtFish = null;

        m_OnRetract = onRetract;

        m_InitialPosition = initialPosition;
        m_Rigidbody.transform.position = m_InitialPosition;

        gameObject.SetActive(true);

        m_Rigidbody.velocity = initialVelocity;

        m_PrevPos = m_InitialPosition;

        m_Positions.Clear();

        m_Positions.Add(m_InitialPosition);
        m_Positions.Add(m_InitialPosition);

        m_LineRenderer.positionCount = m_Positions.Count;

        m_LineRenderer.SetPositions(m_Positions.ToArray());
    }

    

    private void Update()
    {
        Position = m_Rigidbody.transform.position;

        if (m_IsRetracting)
        {
            m_Positions.RemoveAt(m_Positions.Count - 1);


            if(m_Positions.Count == 0)
            {
                m_OnRetract?.Invoke(m_CaughtFish);

                m_Whirr.Stop();

                return;
            }

            var theta = m_Positions.Count / (float)m_prevLength;

            m_Whirr.pitch = 1.5f + (1f - theta) * 0.5f;
            m_Whirr.volume = 0.5f + (1f - theta) * 0.5f;
            m_LineRenderer.positionCount = m_Positions.Count;
            m_LineRenderer.SetPositions(m_Positions.ToArray());

            m_Rigidbody.transform.position = m_Positions[m_Positions.Count - 1];

            return;
        }

        if ((m_Rigidbody.transform.position - m_PrevPos).magnitude > m_MaxDist)
        {
            m_PrevPos = m_Rigidbody.transform.position;

            m_Positions.Add(m_PrevPos);

            m_LineRenderer.positionCount = m_Positions.Count;
            m_LineRenderer.SetPositions(m_Positions.ToArray());
        }

        

        m_LineRenderer.SetPosition(m_LineRenderer.positionCount - 1, m_Rigidbody.transform.position);
    }

    public void Retract(Fish fish = null)
    {
        m_Whirr.volume = 0.5f;
        m_Whirr.pitch = 1.5f;
        m_Whirr.Play();

        m_prevLength = m_Positions.Count;

        m_Rigidbody.isKinematic = true;

        m_IsRetracting = true;

        m_CaughtFish = fish;
    }
}
