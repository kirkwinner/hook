﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterAnimator : MonoBehaviour
{
    [SerializeField] private Material m_WaterMaterial;
    [SerializeField] private Texture2D[] m_WaterTextures;

    [SerializeField] private float m_TextureAnimateSpeed = 3f;
    [SerializeField] private float m_TextureScrollSpeedX = 0.2f;
    [SerializeField] private float m_TextureScrollSpeedY = 0.1f;

    private float m_TextureTheta = 0f;
    private float m_TextureOffsetx = 0f;
    private float m_TextureOffsetY = 0f;

    void Update()
    {
        var prevTextureIndex = (int)(((Mathf.Sin(m_TextureTheta * Mathf.PI * 2f) + 1f) / 2f) * m_WaterTextures.Length);

        if (m_TextureAnimateSpeed != 0f)
            m_TextureTheta = (m_TextureTheta + Time.deltaTime / m_TextureAnimateSpeed) % 1f;

        

        var newTextureIndex = (int)(((Mathf.Sin(m_TextureTheta * Mathf.PI * 2f) + 1f) / 2f) * m_WaterTextures.Length);

        
        if(newTextureIndex != prevTextureIndex)
        {
            m_TextureOffsetx = (m_TextureOffsetx + m_TextureScrollSpeedX) % 1f;
            m_TextureOffsetY = (m_TextureOffsetY + m_TextureScrollSpeedY) % 1f;
            m_WaterMaterial.mainTextureOffset = new Vector2(-m_TextureOffsetx, -m_TextureOffsetY);

            m_WaterMaterial.mainTexture = m_WaterTextures[Mathf.Clamp(newTextureIndex, 0, m_WaterTextures.Length - 1)];
        }
    }
}
