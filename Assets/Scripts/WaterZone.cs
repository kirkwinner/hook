﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterZone : MonoBehaviour
{
    [SerializeField] float m_Buoyancy = 1f;
    [SerializeField] float m_Friction = 3f;

    private void OnTriggerEnter(Collider other)
    {
        var bobber = other.GetComponentInParent<Bobber>();

        if(bobber)
        {
            bobber.PlayPlunk();
        }
    }

    private void OnTriggerStay(Collider other)
    {
        other.attachedRigidbody.AddForce(-Physics.gravity * (1f + (transform.position.y - other.transform.position.y) * m_Buoyancy));
        other.attachedRigidbody.velocity -= other.attachedRigidbody.velocity * Time.deltaTime * m_Friction;
    }
}
