﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UVScaler : MonoBehaviour
{
    void Start()
    {
        MeshFilter meshFilter = GetComponent<MeshFilter>();

        Mesh mesh = meshFilter.mesh;
        Vector3[] verts = mesh.vertices;
        Vector2[] uvs = mesh.uv;
        Vector2[] newUvs = new Vector2[uvs.Length];

        float worldSpaceDist = Vector3.Distance(verts[0], verts[1]);
        float uvDist = Vector2.Distance(uvs[0], uvs[1]);
        float ratio = worldSpaceDist / uvDist;
        
        //for(1: some variable; 2: condition (while this is true); 3: increment)
        //{ functionality }

        for (int i = 0; i < uvs.Length; i++)
        {
            newUvs[i] = uvs[i] * ratio * transform.localScale.x;
        }

        mesh.uv = newUvs;
    }
}
